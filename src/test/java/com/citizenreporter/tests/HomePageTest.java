package test.java.com.citizenreporter.tests;

import test.java.com.citizenreporter.pages.HomePage;
import org.testng.annotations.Test;

import java.io.IOException;

public class HomePageTest extends TestBase {
       @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
           HomePage loginO = new HomePage();
           loginO.Url();
           //Verify user is unable to login with an invalid password
           loginO.invalidLoginPassword();
           //Verify user is unable to login with invalid username and password
           loginO.invalidLoginDetails();
           //Verify user is unable to login with an invalid username
           loginO.invalidLoginUsername();
           //Verify user is able to login
           loginO.login();
           //Verify user can logout
           loginO.logout();
    }
    @Test(priority = 2)
    public void reportEvent() throws IOException, InterruptedException {
           HomePage reporter = new HomePage();
           reporter.ReportEvent();
    }

    }
