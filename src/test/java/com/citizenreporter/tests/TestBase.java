package test.java.com.citizenreporter.tests;


import test.java.com.citizenreporter.utilities.Utility;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

public class TestBase {
    public static WebDriver driver;


    @BeforeTest
    public void setUp() throws IOException {
    //Launch Browser using any of the available
        if (Utility.getProperty("browserName").toString().equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/driver/chromedriver.exe");
            driver = new ChromeDriver();


        } else if (Utility.getProperty("browserName").toString().equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", "./src/main/resources/drivers/geckodriver.exe");
            driver = new FirefoxDriver();

        } else if (Utility.getProperty("browserName").toString().equalsIgnoreCase("edge")) {
            System.setProperty("webdriver.edge.driver", "./src/main/resources/drivers/msedgedriver.exe");
            driver = new EdgeDriver();
        }
    }


    @AfterTest
    public void tearDown() {
        driver.close();

    }
}
