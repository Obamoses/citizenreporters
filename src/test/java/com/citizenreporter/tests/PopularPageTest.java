package test.java.com.citizenreporter.tests;

import org.testng.annotations.Test;
import test.java.com.citizenreporter.pages.PopularPage;

import java.io.IOException;

public class PopularPageTest {
    @Test(priority = 4)
    public void PopularPage() throws IOException, InterruptedException {
        PopularPage popular = new PopularPage();
        popular.ViewPopularPage();
    }
}
