package test.java.com.citizenreporter.utilities;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import test.java.com.citizenreporter.tests.TestBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;

public class Utility extends TestBase {
    public static Object getProperty(String key) throws IOException
    {
        FileInputStream file = new FileInputStream("./src/main/resources/config/config.properties");
        Properties property = new Properties();
        property.load(file);
        property.get(key);
        return property.get(key);
    }

    public static String getLocator(String key ) throws IOException {

        FileInputStream file = new FileInputStream("./src/main/resources/config/locators.properties");
        Properties property = new Properties();
        property.load(file);
        return 	property.get(key).toString();
    }
    public static String fetchText(String key ) throws IOException {

        FileInputStream file = new FileInputStream("./src/main/resources/config/text.properties");
        Properties property = new Properties();
        property.load(file);
        return 	property.get(key).toString();
    }
    public static String fn_GetTimeStamp() {
        DateFormat DF = DateFormat.getDateTimeInstance();
        Date dte = new Date();
        //DateFormat DF = DateFormat.getDateTimeInstance();
        //Date dte = new Date();
        String DateValue = DF.format(dte);
        DateValue = DateValue.replaceAll(":", "_");
        DateValue = DateValue.replaceAll(",", "");
        return DateValue;
    }
    public static void captureScreenshot(String screenshotName) {

        try
        {
            TakesScreenshot ts=(TakesScreenshot)driver;

            File source=ts.getScreenshotAs(OutputType.FILE);

            FileUtils.copyFile(source, new File("./Screenshots/"+screenshotName+".png"));

            System.out.println("Screenshot taken at" + fn_GetTimeStamp());
        }
        catch (Exception e)
        {

            System.out.println("Exception while taking screenshot "+e.getMessage());
        }
    }
}
