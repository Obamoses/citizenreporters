package test.java.com.citizenreporter.pages;


import test.java.com.citizenreporter.utilities.Utility;
import org.openqa.selenium.By;
import test.java.com.citizenreporter.tests.TestBase;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class RecentPage  extends TestBase {
    public void viewRecentPage() throws IOException {
        driver.findElement(By.xpath(Utility.getLocator("RecentMenuButton_Xpath"))).click();
        String RecentPageTitle = driver.findElement(By.xpath(Utility.getLocator("RecentMenuButton_Xpath"))).getText();
        assertEquals(RecentPageTitle,"Recent");
    }
}
