package test.java.com.citizenreporter.pages;

import org.openqa.selenium.By;
import test.java.com.citizenreporter.tests.TestBase;
import test.java.com.citizenreporter.utilities.Utility;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class PopularPage  extends TestBase {

    public void ViewPopularPage() throws IOException {
        driver.findElement(By.xpath(Utility.getLocator("PopularPageMenuButton_Xpath"))).click();
        String PopularPageTitle = driver.findElement(By.xpath(Utility.getLocator("PopularEventTitle_Xpath"))).getText();
        assertEquals(PopularPageTitle,"Popular events");
        }
}
