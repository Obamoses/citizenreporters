package test.java.com.citizenreporter.pages;

import org.openqa.selenium.By;
import test.java.com.citizenreporter.tests.TestBase;
import test.java.com.citizenreporter.utilities.Utility;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class HomePage extends TestBase {

    public void Url() throws IOException {
        driver.get(Utility.getProperty("applicationURL").toString());
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    public void signUp() throws IOException {
        driver.findElement(By.xpath(Utility.getLocator("SignupButton_Xpath"))).click();
    }
    public void login() throws IOException{
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).clear();
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).sendKeys(Utility.fetchText("username"));
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).clear();
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).sendKeys(Utility.fetchText("password"));
        driver.findElement(By.xpath(Utility.getLocator("Login_Xpath"))).click();
        //Verify user is able to login
        String toastMsg = driver.findElement(By.className(Utility.getLocator("LoginPopup_className"))).getText();
        assertEquals(toastMsg,"Error: wrong username/password combination");
    }
    public void logout() throws IOException{
        driver.findElement(By.xpath(Utility.getLocator("NavDropdown_Xpath"))).click();
        driver.findElement(By.xpath(Utility.getLocator("logoutButton_Xpath"))).click();
    }

    public void invalidLoginPassword() throws IOException{
        driver.findElement(By.xpath(Utility.getLocator("LoginButton_Xpath"))).click();
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).sendKeys(Utility.fetchText("username"));
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).sendKeys(Utility.fetchText("password_invalid"));
        driver.findElement(By.xpath(Utility.getLocator("Login_Xpath"))).click();
        //Verify user is unable to login
        String toastMsg = driver.findElement(By.className(Utility.getLocator("LoginPopup_className"))).getText();
        assertEquals(toastMsg,"");
    }

    public void invalidLoginUsername() throws IOException{
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).clear();
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).sendKeys(Utility.fetchText("username_invalid"));
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).clear();
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).sendKeys(Utility.fetchText("password"));
        driver.findElement(By.xpath(Utility.getLocator("Login_Xpath"))).click();
        //Verify user is unable to login
        String toastMsg = driver.findElement(By.className(Utility.getLocator("LoginPopup_className"))).getText();
        assertEquals(toastMsg,"Error: wrong username/password combination");
    }
    public void invalidLoginDetails() throws IOException{
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).clear();
        driver.findElement(By.xpath(Utility.getLocator("UsernameField_Xpath"))).sendKeys(Utility.fetchText("username_invalid"));
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).clear();
        driver.findElement(By.xpath(Utility.getLocator("PasswordField_Xpath"))).sendKeys(Utility.fetchText("password_invalid"));
        driver.findElement(By.xpath(Utility.getLocator("Login_Xpath"))).click();
        //Verify user is unable to login
        String toastMsg = driver.findElement(By.className(Utility.getLocator("LoginPopup_className"))).getText();
        assertEquals(toastMsg,"Error: wrong username/password combination");
    }

    public void ReportEvent() throws IOException{
        driver.get(Utility.getProperty("ReportEventUrl").toString());
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.findElement(By.xpath((Utility.getLocator("ReportEvent_Submit_Xpath")))).click();
    }

}

